/* BEGIN ENVIRONMENT CONFIG */

var confOutputDest    = 'public';                         // the base output directory
var confImageDest     = 'public/_img';                     // where to output images
var confScriptDest    = 'public/_js';                      // where to output scripts
var confStyleDest     = 'public/_css';                     // where to output styles
var confIconsTemplate = 'src/css/_iconstemplate/_icons.css';  // where to output icon fonts
var confIconsDest     = 'public/_fonts';                     // where to output icon fonts
var confTemplateDest  = 'public';                         // where to output html pug
var confUrlDest       = 'localhost';  // the local URL of the project

/* END ENVIRONMENT CONFIG */

var changed             = require('gulp-changed');
var browsersync         = require('browser-sync');
var clean               = false;
var gulp                = require('gulp');
var gulpif              = require('gulp-if');
var gulputil            = require('gulp-util');
var pug                 = require('gulp-pug');
var path                = require('path');
var reload              = browsersync.reload;
var rimraf              = require('rimraf');
var postcss             = require('gulp-postcss');
var sourcemaps          = require('gulp-sourcemaps');
var uglify              = require('gulp-uglify');
var iconfont            = require('gulp-iconfont');
var consolidate         = require('gulp-consolidate');
var rename              = require('gulp-rename');
var concat              = require('gulp-concat');
var mainBowerFiles      = require('main-bower-files');
var imagemin            = require('gulp-imagemin');
var pngquant            = require('imagemin-pngquant');
var beep                = require('beepbeep');
var jshint              = require('gulp-jshint');
var plumber             = require('gulp-plumber');

//PostCSS plugins
var pxtorem             = require('postcss-pxtorem');
var precss              = require('precss');
var cssnano             = require('cssnano');
var mqpacker            = require('css-mqpacker');

//Alerta con beep
var onError = function (err) {
	beep([0, 0, 0, 0, 0]);
	gulputil.log(gulputil.colors.green(err));
};

/**
 * Check to see if --vars were set.
 */
process.argv.forEach(function (val) {
	if (val === '--clean') {
		clean = true;
	}
});

/**
 * Remove dist directory.
 */
gulp.task('clean', function (cb) {
	rimraf(confOutputDest, cb);
});

/**
 * Compile css with PostCSS as compressed css.
 */

gulp.task('style', function () {
	var processors = [
		precss,
		pxtorem({
			rootValue: 16,
			unitPrecision: 5,
			propWhiteList: ['font', 'font-size', 'line-height', 'letter-spacing'],
			selectorBlackList: [],
			replace: true,
			mediaQuery: false,
			minPixelValue: 0,
		}),
		mqpacker,
		cssnano({
			convertValues: {
				length: false,
			},
			discardComments: {
				removeAll: true,
			},
		}),
	];
	return gulp.src('./src/css/main.css')
		.pipe(sourcemaps.init())
		.pipe(postcss(processors))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(confStyleDest))
		.pipe(reload({ stream: true }));
});

/**
 * Copy fonts.
 */

gulp.task('copyfonts', function () {
	gulp.src('./src/fonts/**/*.*')
		.pipe(gulp.dest(confIconsDest));
});

/**
 * Pug to html.
 */
gulp.task('pug', function () {
	return gulp.src('./src/pug/*.pug')
			.pipe(plumber({ errorHandler: onError }))
			.pipe(pug({
				pretty: true,
			}))
			.pipe(gulp.dest(confTemplateDest))
			.pipe(reload({ stream:true }));
});

/**
 * Optimize images.
 */
gulp.task('images', function () {
	return gulp.src('./src/img/**')
			.pipe(plumber({ errorHandler: onError }))
			.pipe(imagemin({
				progressive: true,
				svgoPlugins: [{ removeViewBox: false }],
				use: [pngquant()],
			}))
			.pipe(gulp.dest(confImageDest));
});

/**
 * Compress javascript.
 */
gulp.task('scripts', function () {
	return gulp.src('./src/js/*.js')
			.pipe(plumber({ errorHandler: onError }))
			.pipe(jshint())
			.pipe(jshint.reporter('default'))
			.pipe(concat('main.js'))
			.pipe(changed(confScriptDest))
			.pipe(uglify())
			.pipe(gulp.dest(confScriptDest))
			.pipe(reload({ stream: true }));
});

/**
 * Generate javascript brom bower installation.
 */
gulp.task('scripts_vendor', function () {
	var files = mainBowerFiles('**/*.js', { debugging: true });
	files.push('src/js/vendor/*.js');
	return gulp.src(files)
			.pipe(plumber({ errorHandler: onError }))
			.pipe(concat('vendor.js'))
			.pipe(uglify()) //DEV
			.pipe(gulp.dest(confScriptDest))
			.pipe(reload({ stream: true }));
});

/**
 * Generate icon font from svg.
 */
var runTimestamp = Math.round(Date.now() / 1000);
gulp.task('icons_build', function () {
	gulputil.log(gulputil.colors.white('Haciendo iconos...'));
	return gulp.src(['./src/icons/*.svg'])
			.pipe(iconfont({
				fontName: 'iconos',
				appendUnicode: true,
				normalize: true,
				formats: ['ttf', 'eot', 'woff', 'woff2'],
				timestamp: runTimestamp,
				fontHeight: 500,
			}))
			.on('glyphs', function (glyphs) {
				gulputil.log(gulputil.colors.green('glyphs'));
				gulp.src(confIconsTemplate)
						.pipe(consolidate('lodash', {
							glyphs: glyphs,
							fontName: 'iconos',
							fontPath: '../_fonts/',
							className: 'icon',
						}))
						.pipe(gulp.dest('src/css/'));
			})
			.pipe(gulp.dest(confIconsDest)); // set path to export your fonts
});

/**
 * All build tasks.
 */
gulp.task('build', ['icons_build', 'copyfonts', 'style', 'pug', 'images', 'vendor', 'scripts']);

gulp.task('icons', ['icons_build', 'style']);

gulp.task('vendor', ['scripts_vendor']);

/**
 * Watch for chaned files
 */
gulp.task('watch', ['build'], function () {
	browsersync({
		proxy: confUrlDest,
		open: 'external',
	});
	gulp.watch('src/css/*.css', ['style']);
	gulp.watch('src/pug/**/*.pug', ['pug']);
	gulp.watch('src/js/*.js', ['scripts']);
	gulp.watch('src/icons/*.svg', ['icons']);
	gulp.watch('src/fonts/*.*', ['copyfonts']);
	gulp.watch('src/img/*.*', ['images']);
	gulputil.log(gulputil.colors.inverse('Te estoy vigilando...'));
});

/**
 * Default task
 */
gulp.task('default', function () {
	if (clean === true) {
		gulp.start(['clean']);
	} else {
		gulp.start(['watch']);
	}
});
